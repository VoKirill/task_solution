import json
import os
import tensorflow as tf
import tensorflow_hub as hub
import tensorflow_text
import numpy as np
from scipy import spatial
import argparse
from rouge_score import rouge_scorer


class PostProcessor: 
    def __init__(self, path_data, embedder, similarity_treshold, unsimilarity_treshold):
        self.emb_path = embedder
        self.muse = None
        self.data = json.load(open(path_data, 'r'))
        self.sim_tr = similarity_treshold
        self.unsim_tr = unsimilarity_treshold

    def muse_embeddings(self, source, aug, muse):
        aug_embeddings = muse(aug)
        aug_embeddings = np.array(aug_embeddings).tolist()
        source_embedding = muse(source)
        source_embedding = np.array(source_embedding).tolist()[0]
        return source_embedding, aug_embeddings

    def calculate_cossine_sim(self, source, augmented):
        result = []
        for elem in augmented:
            cossine_sim = 1 - spatial.distance.cosine(source, elem)
            result.append(cossine_sim)
        return result

    def filter_unsimilar(self, augmented, cosine_sim, treshold):
        augmented = list(zip(augmented, cosine_sim))
        augmented = list(filter(lambda x:  x[1] >treshold, augmented))
        augmented = list(map(lambda x: x[0], augmented))
        return augmented

    def rougel_calculation(self, source, augmented):
        result = [] 
        scorer = rouge_scorer.RougeScorer(['rougeL'], use_stemmer=False)
        for elem in augmented:
            scores = scorer.score(source, elem)
            result.append(scores['rougeL'].fmeasure)
        return result

    def filter_too_similar(self, augmented, rougel, treshold):
        augmented = list(zip(augmented, rougel))
        augmented = list(filter(lambda x:  x[1] < treshold, augmented))
        augmented = list(map(lambda x: x[0], augmented))
        return augmented

    def answer_formation(self, source, augmented, passed):
        result = {}
        result['passed'] = list(set(passed))
        result['failed'] = list(set(augmented)-set(passed))
        return source, result


    def run_postprocessing(self):
        self.muse = hub.load(self.emb_path)
        
        result = {}
        for key in self.data.keys():
            source = key
            augmented_origin  = self.data[key]
            source_embedding, aug_embeddings = self.muse_embeddings(source, augmented_origin, muse=self.muse)
            cosine_sim = self.calculate_cossine_sim(source_embedding, aug_embeddings)
            augmented = self.filter_unsimilar(augmented_origin, cosine_sim, self.sim_tr)
            rougel = self.rougel_calculation(source, augmented)
            augmented = self.filter_too_similar(augmented, rougel, self.unsim_tr)
            sourse, sent = self.answer_formation(source, augmented_origin, augmented)
            result[source] = sent
        
        with open('answer.json', 'w') as f:
            json.dump(result, f)

        return 1


if __name__ == '__main__':
    parser = argparse.ArgumentParser('solution')
    parser.add_argument('-gpu_num', dest='gpu_num', type=str)
    parser.add_argument('-path_to_data', dest='path_to_data', type=str)
    args = parser.parse_args()
    path_to_data = args.path_to_data
    os.environ['CUDA_VISIBLE_DEVICES']= args.gpu_num
    
    postprocessor = PostProcessor(path_to_data,
                                  embedder="https://tfhub.dev/google/universal-sentence-encoder-multilingual/3",
                                  similarity_treshold=0.7,
                                  unsimilarity_treshold=0.9)
    postprocessor.run_postprocessing()
    